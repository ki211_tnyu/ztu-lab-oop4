using System;
using System.Drawing;
using System.Windows.Forms;

namespace Arrays2DWindowsForm
{
    public partial class Form1 : Form
    {
        double[,] arr;
        double sumPrevColumn = 0, sumCurrentColumn = 0;
        private object numericUpDownColumns;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture =
            customCulture;
        }

       

        private void button_Gener_Click(object sender, EventArgs e)
        {
            int n, m;
            double MIN = -27.8, MAX = 78.4;
            n = Convert.ToInt32(Math.Round(numericUpDown1.Value));
            m = Convert.ToInt32(Math.Round(numericUpDown2.Value));
            arr = new double[n, m];
            dataGridViewMatrix.RowCount = n;
            dataGridViewMatrix.ColumnCount = m;
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    arr[i, j] = Math.Round(rnd.NextDouble() * (MAX - MIN) + MIN, 1);
                    dataGridViewMatrix[j, i].Value = arr[i, j];
                }
            }
            for (int i = 0; i < arr.GetLength(1); i++)
            {
                dataGridViewMatrix.Columns[i].HeaderText = i.ToString();
                dataGridViewMatrix.Columns[i].SortMode =
                DataGridViewColumnSortMode.NotSortable;
            }
            for (int j = 0; j < arr.GetLength(0); j++)
                dataGridViewMatrix.Rows[j].HeaderCell.Value = j.ToString();
        }

       

        private void button_Result_Click(object sender, EventArgs e)
        {
            int n, m, i, j, k = 0;
            bool f;
            f = int.TryParse(Console.ReadLine(), out n);
            f = int.TryParse(Console.ReadLine(), out m);
            double[,] matr = new double[n, m];
            Random rnd = new Random();
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m; j++)
                {
                    matr[i, j] = Math.Round(-27.8 + rnd.NextDouble() * (78.4 + 27.8), 1);
                    Console.Write($"{matr[i, j],8}");
                }
                Console.Write("\n");
            }
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m; j++)
                {
                    if (matr[i, j] < 0) break;
                    else if (j == m + 1) k++;
                }
            }
            textBox1.Text = k.ToString();
            for (m = 0; m < matr.GetLength(1); m++)
            {
                for (j = 1; j < matr.GetLength(1); j++)
                {


                    for (i = 0; i < matr.GetLength(0); i++)
                    {
                        sumPrevColumn += matr[i, j - 1];
                    }


                    for (i = 0; i < matr.GetLength(0); i++)
                    {
                        sumCurrentColumn += matr[i, j];
                    }

                    if (sumPrevColumn < sumCurrentColumn)
                    {
                        for (k = 0; k < matr.GetLength(1); k++)
                        {
                            {
                                double temp = matr[k, j];
                                matr[k, j] = matr[k, j - 1];
                                matr[k, j - 1] = temp;
                            }
                        }
                    }
                }
          
                for (i = 0; i < matr.GetLength(0); i++)
                {
                    for (j = 0; j < matr.GetLength(1); j++)
                    {
                        dataGridViewMatrix.Rows[i].Cells[j].Value = matr[i, j];
                    }
                   
                }
            }
           
                }
        private void dataGridViewMatrix_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            {
                if (e.ColumnIndex == -1 && e.RowIndex > -1)
                {
                    e.PaintBackground(e.CellBounds, true);
                    using (SolidBrush br = new SolidBrush(Color.Black))
                    {
                        StringFormat sf = new StringFormat();
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Center;
                        e.Graphics.DrawString(e.RowIndex.ToString(), e.CellStyle.Font, br,
                        e.CellBounds, sf);
                    }
                    e.Handled = true;
                }
            }
        }
    }
}


