﻿using System;
using System.Text;

namespace Arrays2DConsole
{
    class Program
    {

        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone(); customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.WriteLine("Лабораторна робота №4");
            Console.WriteLine("Виконав: Тишкевич Н.Ю. гр.КІ-21-1(2)");
            Console.WriteLine("Варiант №6");
            Console.WriteLine("Завдання 3");
            int n, m, i, j, k = 0;
            bool f;
            double sumPrevColumn = 0, sumCurrentColumn = 0;
            Console.Write("Введіть кількість рядків n = ");
            f = int.TryParse(Console.ReadLine(), out n);
            Console.Write("Введіть кількість стовпців m = ");
            f = int.TryParse(Console.ReadLine(), out m);
            double[,] matr = new double[n, m];
            Random rnd = new Random();
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m; j++)
                {
                    matr[i, j] = Math.Round(-27.8 + rnd.NextDouble() * (78.4 + 27.8), 1);
                    Console.Write($"{matr[i, j],8}");
                }
                Console.Write("\n");
            }
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m; j++)
                {
                    if (matr[i, j] < 0) break;
                    else if (j == m + 1) k++;
                }
            }
            Console.WriteLine($"Кількість рядків, які не містять жодного додатнього елемента ={ k }");
            for (m = 0; m < matr.GetLength(1); m++)
            {
                for (j = 1; j < matr.GetLength(1); j++)
                {
                  
                    
                    for (i = 0; i < matr.GetLength(0); i++)
                    {
                        sumPrevColumn += matr[i , j-1];
                    }
                 
                    
                    for (i = 0; i < matr.GetLength(0); i++)
                    {
                        sumCurrentColumn += matr[i, j];
                    }
                
                    if (sumPrevColumn < sumCurrentColumn)
                    {
                        for (k = 0; k < matr.GetLength(1); k++)
                        {
                                    {
                                        double temp = matr[k, j];
                                        matr[k, j] = matr[k, j - 1];
                                        matr[k, j - 1] = temp;
                                    }
                        }
                    }
                }
            }
            Console.WriteLine();
            for (i = 0; i < matr.GetLength(0); i++)
            {
                for (j = 0; j < matr.GetLength(1); j++)
                {
                    Console.Write($"{matr[i, j],8} ");
                }
                Console.WriteLine();

            }
            }
        }
    }

  
    
