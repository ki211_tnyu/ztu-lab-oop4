﻿namespace Arrays1DWindowsForm
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownCount = new System.Windows.Forms.NumericUpDown();
            this.button_gener = new System.Windows.Forms.Button();
            this.button_result = new System.Windows.Forms.Button();
            this.dataGridViewArray = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSum = new System.Windows.Forms.TextBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArray)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Pink;
            this.label1.Location = new System.Drawing.Point(30, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Кількість елементів:";
            // 
            // numericUpDownCount
            // 
            this.numericUpDownCount.Location = new System.Drawing.Point(208, 26);
            this.numericUpDownCount.Name = "numericUpDownCount";
            this.numericUpDownCount.Size = new System.Drawing.Size(86, 27);
            this.numericUpDownCount.TabIndex = 1;
            this.numericUpDownCount.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // button_gener
            // 
            this.button_gener.BackColor = System.Drawing.Color.PeachPuff;
            this.button_gener.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_gener.ForeColor = System.Drawing.Color.DeepPink;
            this.button_gener.Location = new System.Drawing.Point(376, 26);
            this.button_gener.Name = "button_gener";
            this.button_gener.Size = new System.Drawing.Size(103, 29);
            this.button_gener.TabIndex = 2;
            this.button_gener.Text = "Генерувати";
            this.button_gener.UseVisualStyleBackColor = false;
            this.button_gener.Click += new System.EventHandler(this.button_gener_Click);
            // 
            // button_result
            // 
            this.button_result.BackColor = System.Drawing.Color.PeachPuff;
            this.button_result.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_result.ForeColor = System.Drawing.Color.DeepPink;
            this.button_result.Location = new System.Drawing.Point(546, 26);
            this.button_result.Name = "button_result";
            this.button_result.Size = new System.Drawing.Size(104, 29);
            this.button_result.TabIndex = 3;
            this.button_result.Text = "Розв\'язати";
            this.button_result.UseVisualStyleBackColor = false;
            this.button_result.Click += new System.EventHandler(this.button_result_Click);
            // 
            // dataGridViewArray
            // 
            this.dataGridViewArray.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArray.Location = new System.Drawing.Point(30, 68);
            this.dataGridViewArray.Name = "dataGridViewArray";
            this.dataGridViewArray.RowHeadersWidth = 51;
            this.dataGridViewArray.RowTemplate.Height = 29;
            this.dataGridViewArray.Size = new System.Drawing.Size(620, 250);
            this.dataGridViewArray.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Pink;
            this.label2.Location = new System.Drawing.Point(30, 360);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(237, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Сума доданих елементів масиву:";
            // 
            // textBoxSum
            // 
            this.textBoxSum.Location = new System.Drawing.Point(326, 360);
            this.textBoxSum.Name = "textBoxSum";
            this.textBoxSum.Size = new System.Drawing.Size(125, 27);
            this.textBoxSum.TabIndex = 6;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.ForeColor = System.Drawing.Color.Pink;
            this.linkLabel1.LinkColor = System.Drawing.Color.Pink;
            this.linkLabel1.Location = new System.Drawing.Point(417, 388);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(249, 25);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Tyshkevych Nazarii KI-21-1(2)";
            this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(697, 419);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.textBoxSum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridViewArray);
            this.Controls.Add(this.button_result);
            this.Controls.Add(this.button_gener);
            this.Controls.Add(this.numericUpDownCount);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Crimson;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Лабораторна робота №4. Завдання 1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArray)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private NumericUpDown numericUpDownCount;
        private Button button_gener;
        private Button button_result;
        private DataGridView dataGridViewArray;
        private Label label2;
        private TextBox textBoxSum;
        private LinkLabel linkLabel1;
    }
}