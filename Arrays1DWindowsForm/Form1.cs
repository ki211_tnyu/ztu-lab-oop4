using System;
using System.Windows.Forms;

namespace Arrays1DWindowsForm
{
    public partial class Form1 : Form
    {
        double[] arr;
        int maxi = 0, mini = 0;
        bool fl;
        double mass;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture =
            (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture =
            customCulture;
        }

        private void button_gener_Click(object sender, EventArgs e)
        {
            int n;
            double MAX = 110.35, MIN = -110.34;
            n = Convert.ToInt32(Math.Round(numericUpDownCount.Value));
            arr = new double[n];
            dataGridViewArray.RowCount = 1;
            dataGridViewArray.ColumnCount = n;

            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = Math.Round(MIN+rnd.NextDouble() * (MAX - MIN), 2);
                dataGridViewArray[i, 0].Value = arr[i];
                dataGridViewArray.Columns[i].HeaderText = i.ToString();
            }
        }

        private void button_result_Click(object sender, EventArgs e)
        {
            int n = Convert.ToInt32(Math.Round(numericUpDownCount.Value));
            double max = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    maxi = i;
                }
            }
            double min = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                    mini = i;
                }
            }
            double sum = 0;
            for (int i = 0; i < n; i++)
            {
                if (arr[i] > 0)
                {
                    sum = Math.Round(min + max, 2);
                }
            }
            textBoxSum.Text = sum.ToString();
            do
            {
                fl = false;
                for (int i = 0; i < mini; i++)
                    if (arr[i + 1] < arr[i])
                    {
                        mass = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = mass;
                        fl = true;
                        break;
                    }
            } while (fl);
            for (int i = 0; i < arr.Length; i++)
            {
                dataGridViewArray[i, 0].Value = arr[i];
            }
        }
    }
}