﻿using System;
using System.Text;

namespace Arrays1DConsole
{
    class Program
    {
        
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone(); customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.WriteLine("Лабораторна робота №4");
            Console.WriteLine("Виконав: Тишкевич Н.Ю. КІ-21-1(2)");
            Console.WriteLine("Варiант №6");
            Console.WriteLine("Завдання 1");
            Random rnd = new Random();
            int n;
            bool f,fl;
            int maxi = 0, mini = 0;
            double mass;
            do
            {
                Console.Write("Введіть кількість елементів: ");
                f = int.TryParse(Console.ReadLine(), out n);
                if (f == false)
                    Console.WriteLine(" Помилка! Введено некоректне значення");
            } while (!f);
            double[] arr = new double[n];
            for (int i = 0; i < n; i++)
            {
                arr[i] = Math.Round(-110.34 + rnd.NextDouble() * (110.35 - (-110.34)), 2);
                Console.Write("{0,14}", arr[i]);
            }
            double max = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    maxi = i;
                }
            }
            double min = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                    mini = i;
                }
            }
            double sum = 0;
            for (int i = 0; i < n; i++)''2  Й
            {
                if (arr[i] > 0)
                {
                    sum = Math.Round(min + max, 2);
                }
            }
            Console.WriteLine($"\nСума максимального та мінімального елементів ={ sum }");
            Console.WriteLine("Відсортований масив");
            do
            {
                fl = false;
                for (int i = 0; i < mini; i++)
                    if (arr[i + 1] < arr[i])
                    {
                        mass = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = mass;
                        fl = true;
                        break;
                    }
            } while (fl);
            for(int i=0;i<arr.Length;i++)
            {
                Console.Write("{0,14}",arr[i]);
            }
        }

    }
}

